// ! Importing Modules:

// ? This is called "importing modules" or "require statements" in Node.js. The code is using the require() function to load and use modules (i.e., express, mongoose, cors, dotenv) in the current script. These modules provide additional functionality that is not available in the core Node.js runtime.

// * This line imports the express module and assigns it to the constant variable express.
const express = require("express");

// * This line imports the mongoose module and assigns it to the constant variable mongoose. Mongoose is an Object Data Modeling (ODM) library for MongoDB and Node.js. It provides a powerful schema-based solution to model your application data.
const mongoose = require("mongoose");

// * This line imports the cors module and assigns it to the constant variable cors. CORS (Cross-Origin Resource Sharing) is a security feature implemented by web browsers that blocks web pages from making requests to a different domain than the one that served the web page. This is done to prevent malicious websites from stealing sensitive information from other websites

// * In Node.js, CORS can be implemented using a middleware package such as cors. It allows you to configure the server to allow or disallow specific origins and HTTP methods. This can be useful if you are building a web application that makes API calls to a server running on a different domain.
const cors = require("cors");

// * This line imports the dotenv module and assigns it to the constant variable dotenv. It also calls the config() method on it to configure environment variables from a .env file. dotenv is a zero-dependency module that loads environment variables from a .env file.
const dotenv = require("dotenv").config();
const bcrypt = require("bcrypt");

// ! Routes

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// ! Connect Express

const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// ! Connect Mongoose

mongoose.connect(`mongodb+srv://ronb077:${process.env.PASSWORD}@cluster0.umwequr.mongodb.net/course-booking-api?retryWrites=true&w=majority`, {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB"));

app.use("/users", userRoutes);
app.use("/course", courseRoutes);


app.listen(port, () => console.log(`API is now online on port ${port}`));




