const Course = require("../models/course");

module.exports.addCourse = (data) => {
  // Uses the information from the request body to provide all the necessary information.

  if (data.isAdmin) {
    let new_course = new Course({
      name: data.course.name,
      description: data.course.description,
      price: data.course.price,
    });

    return new_course.save().then((new_course, error) => {
      if (error) {
        return false;
      }

      return {
        message: "New course successfully created!",
      };
    });
  }

  let message = Promise.resolve("User must me Admin to Access this.");

  return message.then((value) => {
    return value;

    // you can use this to simplify code
    // Promise.resolve('User must me Admin to Access this.')
  });
};

module.exports.getCourses = () => {
  return Course.find({}).then((result) => {
    return result;
  });
};

module.exports.updateCourse = (reqParams, reqBody) => {
  // specify the fileds / properties of the document to be updated

  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };

  /*
		Syntax:

		findByIdAndUpdate(document ID, updatesToBeApplied)
	*/

  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return {
          message: "Course updated successfully",
        };
      }
    }
  );
};

module.exports.archiveCourse = (reqParams) => {
  let updateActiveField = {
    isActive: false,
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return {
          message: "Course status updated successfully",
        };
      }
    }
  );
};

module.exports.unarchiveCourse = (reqParams) => {
  let updateActiveField = {
    isActive: true,
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return {
          message: "Course status updated successfully",
        };
      }
    }
  );
};

module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then((result) => {
    return result;
  });
};
