const express = require("express");
const router = express.Router();
const auth = require("../auth");

// import directory

const courseController = require("../controllers/courseController");

// Router for creating a course

router.post("/addCourse", auth.verify, (req, res) => {
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  courseController.addCourse(data).then((result) => res.send(result));
});

//[Activity]

router.get("/getCourses", (req, res) => {
  courseController
    .getCourses()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for updating the course

router.put("/:courseId", auth.verify, (req, res) => {
  courseController
    .updateCourse(req.params, req.body)
    .then((result) => res.send(result));
});

//routers for archiving courses

//Using Params
router.put("/archive/:courseId", auth.verify, (req, res) => {
  courseController.archiveCourse(req.params).then((result) => res.send(result));
});

router.put("/unarchive/:courseId", auth.verify, (req, res) => {
  courseController
    .unarchiveCourse(req.params)
    .then((result) => res.send(result));
});

router.get("/getAllActive", auth.verify, (req, res) => {
  courseController.getAllActive().then((result) => res.send(result));
});

module.exports = router;
